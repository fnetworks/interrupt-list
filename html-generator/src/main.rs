use askama::Template;
use clap::{App, Arg};
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::{params, Connection};
use serde_json;
use std::error::Error;
use std::fs::{self, File};
use std::io::{self, Write};
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::sync::Arc;

mod templates;
use templates::*;

mod text;
use text::{CATEGORY_MAP, TAG_MAP};

const INTERRUPTS_DIRNAME: &str = "interrupts";
const CATEGORIES_DIRNAME: &str = "categories";

fn copy_file_dir(source_dir: &Path, target_dir: &Path, filename: &str) -> io::Result<u64> {
    fs::copy(source_dir.join(filename), target_dir.join(filename))
}

fn copy_assets(out_dir: &Path, assets_dir: &Path) -> io::Result<()> {
    println!("START copy_assets");
    fs::create_dir_all(out_dir)?;

    copy_file_dir(&assets_dir, out_dir, "nav.js")?;

    copy_file_dir(&assets_dir, out_dir, "site.webmanifest")?;

    copy_file_dir(&assets_dir, out_dir, "android-chrome-192x192.png")?;
    copy_file_dir(&assets_dir, out_dir, "android-chrome-512x512.png")?;

    copy_file_dir(&assets_dir, out_dir, "apple-touch-icon.png")?;

    copy_file_dir(&assets_dir, out_dir, "favicon-16x16.png")?;
    copy_file_dir(&assets_dir, out_dir, "favicon-32x32.png")?;
    copy_file_dir(&assets_dir, out_dir, "favicon.ico")?;

    println!("DONE copy_assets");
    Ok(())
}

/// Generates /index.html
fn generate_index(out_dir: &Path, base_url: Option<&str>) -> Result<(), Box<dyn Error>> {
    println!("START generate_index");
    let index_template = IndexTemplate { base_url };

    fs::create_dir_all(out_dir)?;

    let index_file = out_dir.join("index.html");
    let mut index_file = File::create(index_file)?;
    index_file.write_all(index_template.render()?.as_bytes())?;

    println!("DONE generate_index");
    Ok(())
}

/// Generates /about.html
fn generate_about(out_dir: &Path, base_url: Option<&str>) -> Result<(), Box<dyn Error>> {
    println!("START generate_about");
    let about_template = AboutTemplate { base_url };

    fs::create_dir_all(out_dir)?;

    let about_file = out_dir.join("about.html");
    let mut about_file = File::create(about_file)?;
    about_file.write_all(about_template.render()?.as_bytes())?;

    println!("DONE generate_about");
    Ok(())
}

/// Generates /interrupts/*/*.html
fn generate_interrupts<'a>(
    s: &rayon::Scope<'a>,
    out_dir: &Path,
    base_url: Option<&'a str>,
    pool: &r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>,
) -> Result<(), Box<dyn Error>> {
    println!("START generate_interrupts");
    let interrupts_dir = out_dir.join(INTERRUPTS_DIRNAME);

    let conn = pool.get().unwrap();
    let mut stmt = conn.prepare("SELECT * FROM Interrupts")?;
    let interrupts_iter = stmt.query_map(params![], |row| {
        let category: String = row.get("category")?;
        Ok(Interrupt {
            id: row.get("entry_id")?,
            interrupt: row.get("interrupt")?,
            breadcrumb: serde_json::from_str(&row.get::<_, String>("breadcrumb")?).unwrap(),
            tags: row
                .get::<_, Option<String>>("tags")?
                .map(|x| {
                    serde_json::from_str::<Vec<String>>(&x)
                        .unwrap()
                        .into_iter()
                        .map(|y| TAG_MAP.get(y.as_str()).unwrap())
                        .collect()
                })
                .unwrap_or_else(Vec::new),
            category: CATEGORY_MAP.get(category.as_str()).unwrap(),
            category_key: category,
            constraints: row
                .get::<_, Option<String>>("reg_stack_content")?
                .as_deref()
                .map(|s| serde_json::from_str(s).unwrap())
                .unwrap_or_else(Vec::new),
            description: row.get("field__description")?,
            notes: row
                .get::<_, Option<String>>("field__notes")?
                .map(|x| x.split('\n').map(str::to_owned).collect()),
            returns: row.get("field__returns")?,
            bugs: row
                .get::<_, Option<String>>("field__bugs")?
                .map(|x| x.split('\n').map(str::to_owned).collect()),
            program: row.get("field__program")?,
            install_check: row.get("field__install_check")?,
            index: row.get("field__index")?,
            example: row.get("field__example")?,
            range: row.get("field__range")?,
            warning: row.get("field__warning")?,
            see_also: row
                .get::<_, Option<_>>("field__see_also")?
                .map(|x: String| serde_json::from_str(&x).unwrap()),
            tables: Vec::new(),
        })
    })?;

    let interrupts_dir = Arc::new(interrupts_dir);

    for interrupt in interrupts_iter {
        let interrupts_dir = interrupts_dir.clone();
        let mut interrupt = interrupt?;
        let pool = pool.clone();
        s.spawn(move |_| {
            let conn = pool.get().unwrap();
            let mut table_stmt = conn
                .prepare("SELECT * FROM Tables WHERE parent_item = ?1")
                .unwrap();
            let table_iter = table_stmt
                .query_map(params![interrupt.id], |table_row| {
                    Ok(Table {
                        id: table_row.get("id")?,
                        title: table_row.get("title")?,
                        header: table_row
                            .get::<_, Option<String>>("header")?
                            .as_deref()
                            .map(serde_json::from_str)
                            .transpose()
                            .unwrap(),
                        rows: serde_json::from_str(&table_row.get::<_, String>("rows")?).unwrap(),
                        see_also: table_row
                            .get::<_, Option<String>>("field__see_also")?
                            .as_deref()
                            .map(serde_json::from_str)
                            .transpose()
                            .unwrap(),
                        notes: table_row.get("field__notes")?,
                        returns: table_row.get("field__returns")?,
                        index: table_row.get("field__index")?,
                        description: table_row.get("field__description")?,
                        bugs: table_row.get("field__bugs")?,
                        program: table_row.get("field__program")?,
                        legend: table_row.get("field__legend")?,
                    })
                })
                .unwrap();
            interrupt.tables = table_iter.map(|x| x.unwrap()).collect();
            let int_dir = interrupts_dir.join(format!("{:02X}", interrupt.interrupt));
            fs::create_dir_all(&int_dir).unwrap();
            let int_file = int_dir.join(format!("{:04}.html", interrupt.id));

            let template = InterruptTemplate {
                base_url,
                interrupt,
            };

            let mut int_file = File::create(int_file).unwrap();
            int_file
                .write_all(template.render().unwrap().as_bytes())
                .unwrap();
        });
    }

    println!("DONE generate_interrupts");
    Ok(())
}

/// Generate /interrupts/*/index.html
fn generate_interrupt_indices(
    out_dir: &Path,
    base_url: Option<&str>,
    conn: &Connection,
) -> Result<(), Box<dyn Error>> {
    println!("START generate_interrupt_indices");
    let interrupts_dir = out_dir.join(INTERRUPTS_DIRNAME);

    let mut stmt = conn.prepare("SELECT * FROM Interrupts WHERE interrupt = ?")?;

    for interrupt in 0..=255 {
        let int_dir = interrupts_dir.join(format!("{:02X}", interrupt));
        fs::create_dir_all(&int_dir)?;

        let int_iter = stmt.query_map(params![interrupt], |row| {
            Ok(IndexInterrupt {
                interrupt: row.get("interrupt")?,
                id: row.get("entry_id")?,
                breadcrumb: serde_json::from_str(&row.get::<_, String>("breadcrumb")?).unwrap(),
            })
        })?;

        let template = InterruptIndexTemplate {
            base_url,
            interrupt,
            interrupts: int_iter.collect::<Result<_, _>>()?,
        };

        let out_file = int_dir.join("index.html");
        let mut out_file = File::create(out_file)?;
        out_file.write_all(template.render()?.as_bytes())?;
    }

    println!("DONE generate_interrupt_indices");
    Ok(())
}

/// Generate /interrupts/categories/*.html
fn generate_category_indices(
    out_dir: &Path,
    base_url: Option<&str>,
    conn: &Connection,
) -> Result<(), Box<dyn Error>> {
    println!("START generate_category_indices");
    let category_dir = out_dir.join(INTERRUPTS_DIRNAME).join(CATEGORIES_DIRNAME);
    fs::create_dir_all(&category_dir)?;

    let mut stmt = conn.prepare("SELECT * FROM Interrupts WHERE category = ?1")?;
    for (category_key, category) in CATEGORY_MAP.iter() {
        let int_iter = stmt.query_map(params![category_key], |row| {
            Ok(IndexInterrupt {
                interrupt: row.get("interrupt")?,
                id: row.get("entry_id")?,
                breadcrumb: serde_json::from_str(&row.get::<_, String>("breadcrumb")?).unwrap(),
            })
        })?;

        let template = CategoryIndexTemplate {
            base_url,
            category,
            interrupts: int_iter.collect::<Result<_, _>>()?,
        };

        let out_file = category_dir.join(format!("{}.html", category_key));
        let mut out_file = File::create(out_file)?;
        out_file.write_all(template.render()?.as_bytes())?;
    }

    println!("DONE generate_category_indices");
    Ok(())
}

/// Generate /interrupts/by_number.html
fn generate_interrupt_by_number(
    out_dir: &Path,
    base_url: Option<&str>,
) -> Result<(), Box<dyn Error>> {
    println!("START generate_interrupt_by_number");
    let interrupts_dir = out_dir.join(INTERRUPTS_DIRNAME);
    fs::create_dir_all(&interrupts_dir)?;

    let template = ByNumberTemplate { base_url };

    let out_file = interrupts_dir.join("by_number.html");
    let mut out_file = File::create(out_file)?;
    out_file.write_all(template.render()?.as_bytes())?;

    println!("DONE generate_interrupt_by_number");
    Ok(())
}

/// Generate /interrupts/by_category.html
fn generate_interrupt_by_category(
    out_dir: &Path,
    base_url: Option<&str>,
) -> Result<(), Box<dyn Error>> {
    println!("START generate_interrupt_by_category");

    let interrupts_dir = out_dir.join(INTERRUPTS_DIRNAME);
    fs::create_dir_all(&interrupts_dir)?;

    let template = ByCategoryTemplate {
        base_url,
        categories: &CATEGORY_MAP,
    };

    let out_file = interrupts_dir.join("by_category.html");
    let mut out_file = File::create(out_file)?;
    out_file.write_all(template.render()?.as_bytes())?;

    println!("DONE generate_interrupt_by_category");
    Ok(())
}

fn main() {
    let matches = App::new("html-generator")
        .arg(
            Arg::with_name("DB")
                .required(true)
                .help("the source database"),
        )
        .arg(
            Arg::with_name("output")
                .long("output")
                .short("o")
                .required(true)
                .takes_value(true)
                .help("the output directory"),
        )
        .arg(
            Arg::with_name("asset_dir")
                .long("assets")
                .takes_value(true)
                .default_value("assets")
                .help("the asset directory"),
        )
        .arg(
            Arg::with_name("base")
                .long("base")
                .takes_value(true)
                .help("the base URL for HTML pages"),
        )
        .get_matches();

    let database = PathBuf::from(matches.value_of("DB").unwrap());
    let output_dir = PathBuf::from(matches.value_of("output").unwrap());
    let asset_dir = PathBuf::from(matches.value_of("asset_dir").unwrap());
    let base_url = matches.value_of("base");

    let manager = SqliteConnectionManager::file(database);
    let pool = r2d2::Pool::new(manager).unwrap();

    rayon::scope(move |s| {
        let output_dir = Arc::new(output_dir);
        {
            let output_dir = output_dir.clone();
            s.spawn(move |_| copy_assets(&output_dir, &asset_dir).unwrap());
        }
        {
            let output_dir = output_dir.clone();
            s.spawn(move |_| generate_index(&output_dir, base_url).unwrap());
        }
        {
            let output_dir = output_dir.clone();
            s.spawn(move |_| generate_about(&output_dir, base_url).unwrap());
        }
        {
            let pool = pool.clone();
            let output_dir = output_dir.clone();
            s.spawn(move |s1| generate_interrupts(s1, &output_dir, base_url, &pool).unwrap());
        }
        {
            let pool = pool.clone();
            let output_dir = output_dir.clone();
            s.spawn(move |_| {
                generate_interrupt_indices(&output_dir, base_url, pool.get().unwrap().deref())
                    .unwrap()
            });
        }
        {
            let pool = pool.clone();
            let output_dir = output_dir.clone();
            s.spawn(move |_| {
                generate_category_indices(&output_dir, base_url, pool.get().unwrap().deref())
                    .unwrap()
            });
        }
        {
            let output_dir = output_dir.clone();
            s.spawn(move |_| generate_interrupt_by_number(&output_dir, base_url).unwrap());
        }
        {
            let output_dir = output_dir.clone();
            s.spawn(move |_| generate_interrupt_by_category(&output_dir, base_url).unwrap());
        }
    });
}
