use lazy_static::lazy_static;
use std::collections::HashMap;

use crate::templates::{Category, Tag};

lazy_static! {
    pub static ref TAG_MAP: HashMap<&'static str, Tag<'static>> = {
        let mut map = HashMap::new();
        map.insert(
            "Undocumented",
            Tag {
                text: "Undocumented",
                tooltip: "Undocumented function",
                ttype: "danger",
            },
        );
        map.insert(
            "PartiallyDocumented",
            Tag {
                text: "Partially documented",
                tooltip: "Partially documented function",
                ttype: "warning",
            },
        );
        map.insert(
            "AvailableOnlyInProtectedMode",
            Tag {
                text: "Protected Mode",
                tooltip: "Available only in Protected Mode",
                ttype: "info",
            },
        );
        map.insert(
            "AvailableOnlyInRealOrV86Mode",
            Tag {
                text: "Real/V86 Mode",
                tooltip: "Available only in Real or V86 Mode",
                ttype: "info",
            },
        );
        map.insert(
            "CalloutOrCallback",
            Tag {
                text: "Callout or Callback",
                tooltip: "Callout or Callback (usually hooked rather than called)",
                ttype: "info",
            },
        );
        map.insert(
            "Obsolete",
            Tag {
                text: "Obsolete",
                tooltip: "Obsolete (no longer present in current versions)",
                ttype: "info",
            },
        );
        map
    };
    pub static ref CATEGORY_MAP: HashMap<&'static str, Category<'static>> = {
        let mut map = HashMap::new();
        map.insert(
            "Applications",
            Category {
                short_text: None,
                text: "Applications",
            },
        );
        map.insert(
            "AccessSoftware",
            Category {
                short_text: Some("Access software"),
                text: "Access software (screen readers, etc)",
            },
        );
        map.insert(
            "Bios",
            Category {
                short_text: None,
                text: "BIOS",
            },
        );
        map.insert(
            "VendorSpecificBiosExtensions",
            Category {
                short_text: None,
                text: "Vendor-specific BIOS extensions",
            },
        );
        map.insert(
            "CpuGenerated",
            Category {
                short_text: None,
                text: "CPU-generated",
            },
        );
        map.insert(
            "Caches",
            Category {
                short_text: None,
                text: "Caches/spoolers",
            },
        );
        map.insert(
            "DosKernel",
            Category {
                short_text: None,
                text: "DOS kernel",
            },
        );
        map.insert(
            "DiskIoEnhancements",
            Category {
                short_text: None,
                text: "Disk I/O enhancements",
            },
        );
        map.insert(
            "DosExtenders",
            Category {
                short_text: None,
                text: "DOS extenders",
            },
        );
        map.insert(
            "ElectronicMail",
            Category {
                short_text: None,
                text: "Electronic mail",
            },
        );
        map.insert(
            "Fax",
            Category {
                short_text: None,
                text: "FAX",
            },
        );
        map.insert(
            "FileManipulation",
            Category {
                short_text: None,
                text: "File manipulation",
            },
        );
        map.insert(
            "DebuggingTools",
            Category {
                short_text: None,
                text: "Debuggers/debugging tools",
            },
        );
        map.insert(
            "Games",
            Category {
                short_text: None,
                text: "Games",
            },
        );
        map.insert(
            "Hardware",
            Category {
                short_text: None,
                text: "Hardware",
            },
        );
        map.insert(
            "VendorSpecificHardware",
            Category {
                short_text: None,
                text: "Vendor-specific hardware",
            },
        );
        map.insert(
            "IBM",
            Category {
                short_text: None,
                text: "IBM workstation/terminal emulators",
            },
        );
        map.insert(
            "SystemInfo",
            Category {
                short_text: None,
                text: "System info/monitoring",
            },
        );
        map.insert(
            "Japanese",
            Category {
                short_text: None,
                text: "Japanese",
            },
        );
        map.insert(
            "JokePrograms",
            Category {
                short_text: None,
                text: "Joke programs",
            },
        );
        map.insert(
            "KeyboardEnhancers",
            Category {
                short_text: None,
                text: "Keyboard enhancers",
            },
        );
        map.insert(
            "Compression",
            Category {
                short_text: None,
                text: "File/disk compression",
            },
        );
        map.insert(
            "Shells",
            Category {
                short_text: None,
                text: "Shells/command interpreters",
            },
        );
        map.insert(
            "Mouse",
            Category {
                short_text: None,
                text: "Mouse/pointing device",
            },
        );
        map.insert(
            "MemoryManagement",
            Category {
                short_text: None,
                text: "Memory management",
            },
        );
        map.insert(
            "Network",
            Category {
                short_text: None,
                text: "Network",
            },
        );
        map.insert(
            "NonTraditionalInputDevices",
            Category {
                short_text: None,
                text: "Non-traditional input devices",
            },
        );
        map.insert(
            "OtherOperatingSystems",
            Category {
                short_text: None,
                text: "Other operating systems",
            },
        );
        map.insert(
            "PrinterEnhancements",
            Category {
                short_text: None,
                text: "Printer enhancements",
            },
        );
        map.insert(
            "PowerManagement",
            Category {
                short_text: None,
                text: "Power management",
            },
        );
        map.insert(
            "DESQView",
            Category {
                short_text: Some("DESQview/TopView/Quarterdeck"),
                text: "DESQview/TopView and Quarterdeck programs",
            },
        );
        map.insert(
            "RemoteControl",
            Category {
                short_text: None,
                text: "Remote control/file access",
            },
        );
        map.insert(
            "RuntimeSupport",
            Category {
                short_text: None,
                text: "Runtime support",
            },
        );
        map.insert(
            "SerialIo",
            Category {
                short_text: None,
                text: "Serial I/O",
            },
        );
        map.insert(
            "Sound",
            Category {
                short_text: None,
                text: "Sound/speech",
            },
        );
        map.insert(
            "Multitaskers",
            Category {
                short_text: Some("DOS Multitaskers"),
                text: "DOS-based task switchers/multitaskers",
            },
        );
        map.insert(
            "TsrLibraries",
            Category {
                short_text: None,
                text: "TSR libraries",
            },
        );
        map.insert(
            "ResidentUtilities",
            Category {
                short_text: None,
                text: "Resident utilities",
            },
        );
        map.insert(
            "Emulators",
            Category {
                short_text: None,
                text: "Emulators",
            },
        );
        map.insert(
            "Video",
            Category {
                short_text: None,
                text: "Video",
            },
        );
        map.insert(
            "Virus",
            Category {
                short_text: None,
                text: "Virus/antivirus",
            },
        );
        map.insert(
            "MsWindows",
            Category {
                short_text: None,
                text: "MS Windows",
            },
        );
        map.insert(
            "ExpansionBusBios",
            Category {
                short_text: None,
                text: "Expansion bus BIOSes",
            },
        );
        map.insert(
            "NonVolatileConfigStorage",
            Category {
                short_text: None,
                text: "Non-volatile config storage",
            },
        );
        map.insert(
            "Security",
            Category {
                short_text: None,
                text: "Security",
            },
        );
        map.insert(
            "Reserved",
            Category {
                short_text: Some("Reserved"),
                text: "Reserved (and not otherwise classified)",
            },
        );
        map.insert(
            "Unclassified",
            Category {
                short_text: None,
                text: "Unclassified",
            },
        );
        map
    };
}
