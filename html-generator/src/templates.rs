use askama::Template;
use std::collections::HashMap;

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate<'a> {
    pub base_url: Option<&'a str>,
}

#[derive(Template)]
#[template(path = "about.html")]
pub struct AboutTemplate<'a> {
    pub base_url: Option<&'a str>,
}

pub struct Category<'a> {
    pub short_text: Option<&'a str>,
    pub text: &'a str,
}

pub struct Tag<'a> {
    pub tooltip: &'a str,
    pub text: &'a str,
    pub ttype: &'a str,
}

pub struct Table {
    pub id: u32,
    pub title: String,
    pub header: Option<Vec<String>>,
    pub rows: Vec<Vec<String>>,
    pub see_also: Option<Vec<String>>,
    pub notes: Option<String>,
    pub returns: Option<String>,
    pub index: Option<String>,
    pub description: Option<String>,
    pub bugs: Option<String>,
    pub program: Option<String>,
    pub legend: Option<String>,
}

pub struct Interrupt {
    pub id: u32,
    pub interrupt: u8,
    pub breadcrumb: Vec<String>,
    pub constraints: Vec<String>,
    pub tags: Vec<&'static Tag<'static>>,
    pub category_key: String,
    pub category: &'static Category<'static>,
    pub description: Option<String>,
    pub notes: Option<Vec<String>>,
    pub returns: Option<String>,
    pub bugs: Option<Vec<String>>,
    pub program: Option<String>,
    pub install_check: Option<String>,
    pub index: Option<String>,
    pub example: Option<String>,
    pub range: Option<String>,
    pub warning: Option<String>,
    pub see_also: Option<Vec<String>>,
    pub tables: Vec<Table>,
}

#[derive(Template)]
#[template(path = "interrupt.html")]
pub struct InterruptTemplate<'a> {
    pub base_url: Option<&'a str>,
    pub interrupt: Interrupt,
}

pub struct IndexInterrupt {
    pub id: u32,
    pub interrupt: u8,
    pub breadcrumb: Vec<String>,
}

#[derive(Template)]
#[template(path = "interrupt_index.html")]
pub struct InterruptIndexTemplate<'a> {
    pub base_url: Option<&'a str>,
    pub interrupt: u8,
    pub interrupts: Vec<IndexInterrupt>,
}

#[derive(Template)]
#[template(path = "category_index.html")]
pub struct CategoryIndexTemplate<'a> {
    pub base_url: Option<&'a str>,
    pub category: &'static Category<'static>,
    pub interrupts: Vec<IndexInterrupt>,
}

#[derive(Template)]
#[template(path = "by_number.html")]
pub struct ByNumberTemplate<'a> {
    pub base_url: Option<&'a str>,
}

#[derive(Template)]
#[template(path = "by_category.html")]
pub struct ByCategoryTemplate<'a> {
    pub base_url: Option<&'a str>,
    pub categories: &'a HashMap<&'static str, Category<'static>>,
}
