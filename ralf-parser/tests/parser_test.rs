use ralf_parser::{Item, Parser};

const INT_15_67C3_FORMAT_TEST: &str = r#"
----------1567C3CX1Bh
-----------------------
INT 15h - Arabic/Hebrew MS-DOS 5.0???+ - HGC & HGC/RAMFont support???
	AX = 67C3h
	CX = 1Bh
Return: ES:DI -> ???
	AX = ???
"#;

/// INT 15/AX=67C3,CX=1B has an invalid line break and a trailing 'h' in the section header.
#[test]
fn int_15_ax_67c3_invalid_format() {
    let mut parser = Parser::new(INT_15_67C3_FORMAT_TEST, false, 0);

    let intr = parser.next().unwrap().unwrap();
    if let Item::Interrupt(intr) = intr {
        assert_eq!(intr.interrupt, 0x15);
        let add_sel = intr.additional_selector.unwrap();
        assert_eq!(add_sel.ah, Some(0x67));
        assert_eq!(add_sel.al, Some(0xC3));
        assert_eq!(add_sel.ch, Some(0x1B));
        let rsc = intr.reg_stack_content.unwrap();
        assert_eq!(rsc[0], "AX = 67C3h");
        assert_eq!(rsc[1], "CX = 1Bh");
        assert!(intr.fields.returns.is_some());
    } else {
        panic!("Expected interrupt");
    }

    assert!(parser.next().is_none());
}

const TABLE_CONTINUATION_TEST: &str = r#"
--------Q-151016-----------------------------
INT 15 - TopView - "ISOBJ" - VERIFY OBJECT HANDLE

Format of DESQview object:
Offset	Size	Description	(Table 00414)
 02h	WORD	offset in common memory of next object of same type
 04h	WORD	signature FEDCh (DV 2.42-)
		signature FEDCh or object number (DV 2.50+)
 06h	WORD	object type (see #00413)
"#;

#[test]
fn table_continuation() {
    let mut parser = Parser::new(TABLE_CONTINUATION_TEST, false, 0);

    let intr = parser.next().unwrap().unwrap();
    assert!(matches!(intr, Item::Interrupt(_)));

    let table = parser.next().unwrap().unwrap();
    if let Item::Table(table) = table {
        assert_eq!(table.rows.len(), 3);
        assert_eq!(table.rows[0][0], "02h");
        assert_eq!(table.rows[0][1], "WORD");
        assert_eq!(
            table.rows[0][2],
            "offset in common memory of next object of same type"
        );

        assert_eq!(table.rows[1][0], "04h");
        assert_eq!(table.rows[1][1], "WORD");
        assert_eq!(
            table.rows[1][2],
            "signature FEDCh (DV 2.42-)\nsignature FEDCh or object number (DV 2.50+)"
        );

        assert_eq!(table.rows[2][0], "06h");
        assert_eq!(table.rows[2][1], "WORD");
        assert_eq!(table.rows[2][2], "object type (see #00413)");
    } else {
        panic!("Expected table");
    }

    assert!(parser.next().is_none());
}

const FIELD_APPENDING_TEST: &str = r#"
--------Q-151016-----------------------------
INT 15 - TopView - "ISOBJ" - VERIFY OBJECT HANDLE
	AX = 67C3h
Desc: First line of description
	Second line of description
	  and some more text
"#;

#[test]
fn field_appending() {
    let mut parser = Parser::new(FIELD_APPENDING_TEST, false, 0);

    let intr = parser.next().unwrap().unwrap();
    if let Item::Interrupt(intr) = intr {
        let desc_field = intr
            .fields
            .description
            .expect("Expected description to not be None");
        assert_eq!(
            desc_field,
            "First line of description\nSecond line of description and some more text"
        );
    } else {
        panic!("Expected interrupt");
    }
}

// TODO test uppercasing of notes/returns/bugs/etc.
