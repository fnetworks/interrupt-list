use super::fields::FieldCollection;
use super::tokenizer::Tokenizer;
use super::{Category, FieldKey, Fields, Section, Table, Tag};
use crate::error::{ErrorWithState, ParseError};
use std::collections::VecDeque;

#[derive(Debug, PartialEq, Eq)]
enum ParseState {
    Initial,
    MetaSection,
    SectionHeader,
    SectionHeaderAX,
    SectionHeaderNonAX,
    Breadcrumb,
    RegStackContent,
    FieldName,
    FieldText,
    TableStart,
    TableTitle,
    TableTitleNoHeader,
    TableHeader,
    TableBody,
    TableField,
    TableFieldText,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum TableMode {
    Normal,
    Weird,
    WeAreInFieldsButWhyIsThereIndentMissingAndItIsAlsoStartedByLBracketsWhyyyy,
}

pub struct Parser<'a> {
    tokens: Tokenizer<'a>,
    state: ParseState,
    section_counter: u32,
    //verbose: bool,
    pending_tables: VecDeque<Table>,
}

pub enum Item {
    Table(Table),
    Interrupt(Section),
}

impl<'a> Parser<'a> {
    pub fn new(buf: &'a str, _verbose: bool, start_counter: u32) -> Self {
        Self {
            tokens: Tokenizer::new(buf),
            state: ParseState::Initial,
            section_counter: start_counter,
            pending_tables: VecDeque::new(),
            //verbose,
        }
    }
}

fn u8_from_hex(s: &str) -> Result<u8, std::num::ParseIntError> {
    u8::from_str_radix(s, 16)
}

fn u16_from_hex(s: &str) -> Result<u16, std::num::ParseIntError> {
    u16::from_str_radix(s, 16)
}

impl<'a> Parser<'a> {
    fn do_parse_section(&mut self) -> Result<Option<Item>, ParseError> {
        const SECTION_HEADER_START: &str = "--------";

        if let Some(table) = self.pending_tables.pop_front() {
            return Ok(Some(Item::Table(table)));
        }

        let mut section: Option<Section> = None;
        let mut last_field_type: Option<FieldKey> = None;

        let mut table: Option<Table> = None;
        let mut table_header_widths: Option<Vec<u32>> = None;
        let mut last_table_field: Option<String> = None;
        let mut last_row_was_subheading = false;
        let mut tmode = TableMode::Normal;

        'parse: while self.tokens.has_more() {
            //eprintln!("{:?} (offs {})", self.state, self.tokens.current_offset());
            match self.state {
                ParseState::Initial | ParseState::MetaSection => {
                    if self.tokens.is_next_str(SECTION_HEADER_START) {
                        self.state = ParseState::SectionHeader;
                    } else {
                        self.tokens.skip_line();
                    }
                }
                ParseState::SectionHeader => {
                    // Finish last section, if any
                    if let Some(section) = section.take() {
                        return Ok(Some(Item::Interrupt(section)));
                    }

                    self.tokens.expect_str(SECTION_HEADER_START)?;

                    let category = self.tokens.take_char();
                    self.tokens.eat_one_or_more('-')?;

                    match category {
                        '!' => {
                            self.state = ParseState::MetaSection;
                            self.tokens.skip_line();
                        }
                        _ => {
                            let category = Category::from_char(category).unwrap();

                            let interrupt =
                                self.tokens.take_str_map(2, |s| u8::from_str_radix(s, 16))?;

                            section = Some(Section {
                                entry_id: self.section_counter,
                                category,
                                tags: vec![],
                                breadcrumb: vec![],
                                interrupt,
                                additional_selector: None,
                                reg_stack_content: None,
                                fields: Fields::default(),
                            });

                            self.section_counter += 1;
                            self.state = ParseState::SectionHeaderAX;
                        }
                    }
                }
                ParseState::SectionHeaderAX => {
                    if !self.tokens.eat_str("--") {
                        let section = section.as_mut().unwrap();
                        let additional_sel = section
                            .additional_selector
                            .get_or_insert_with(Default::default);
                        additional_sel.ah = Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                        if !self.tokens.eat_str("--") {
                            additional_sel.al =
                                Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                        }
                    } else if !self.tokens.eat_str("--") {
                        let section = section.as_mut().unwrap();
                        let additional_sel = section
                            .additional_selector
                            .get_or_insert_with(Default::default);
                        additional_sel.al = Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                    }
                    self.state = ParseState::SectionHeaderNonAX;
                }
                ParseState::SectionHeaderNonAX => {
                    let section = section.as_mut().unwrap();
                    let additional_sel = section
                        .additional_selector
                        .get_or_insert_with(Default::default);
                    while !self.tokens.is_next_char('-') && !self.tokens.is_next_char('\r') {
                        let reg = self.tokens.take_char();
                        let reg_size = self.tokens.take_char();
                        match (reg, reg_size) {
                            ('B', _) => match reg_size {
                                'H' => {
                                    additional_sel.bh =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap())
                                }
                                'L' => {
                                    additional_sel.bl =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap())
                                }
                                'X' => {
                                    additional_sel.bh =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                                    additional_sel.bl =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                                }
                                'P' => {
                                    // base pointer
                                    additional_sel.bp =
                                        Some(self.tokens.take_str_map(4, |s| {
                                            u16::from_str_radix(s, 16).unwrap()
                                        }));
                                }
                                _ => unimplemented!(
                                    "B{} (offs {})",
                                    reg_size,
                                    self.tokens.current_offset()
                                ),
                            },
                            ('C', _) => {
                                match reg_size {
                                    'H' => {
                                        additional_sel.ch =
                                            Some(self.tokens.take_str_map(2, u8_from_hex).unwrap())
                                    }
                                    'L' => {
                                        additional_sel.cl =
                                            Some(self.tokens.take_str_map(2, u8_from_hex).unwrap())
                                    }
                                    'X' => {
                                        additional_sel.ch =
                                            Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                                        if !(section.interrupt == 0x15
                                            && additional_sel.ah == Some(0x67)
                                            && additional_sel.al == Some(0xC3))
                                        {
                                            additional_sel.cl = Some(
                                                self.tokens.take_str_map(2, u8_from_hex).unwrap(),
                                            );
                                        } else {
                                            // INT 15/AX=67C3h is formatted wrong in v61
                                            self.tokens.expect_char('h')?;
                                            self.tokens.skip_line();
                                            self.tokens.skip_line();
                                            self.state = ParseState::Breadcrumb;
                                            continue 'parse;
                                        }
                                    }
                                    _ => unimplemented!("Register size modifier {:?}", reg_size),
                                }
                            }
                            ('D', _) => match reg_size {
                                'H' => {
                                    additional_sel.dh =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap())
                                }
                                'L' => {
                                    additional_sel.dl =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap())
                                }
                                'X' => {
                                    additional_sel.dh =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                                    additional_sel.dl =
                                        Some(self.tokens.take_str_map(2, u8_from_hex).unwrap());
                                }
                                'I' => {
                                    additional_sel.di =
                                        Some(self.tokens.take_str_map(4, u16_from_hex).unwrap());
                                }
                                'S' => {
                                    additional_sel.ds =
                                        Some(self.tokens.take_str_map(4, u16_from_hex).unwrap());
                                }
                                _ => unimplemented!(
                                    "Register size modifier {:?} (offset {})",
                                    reg_size,
                                    self.tokens.current_offset()
                                ),
                            },
                            ('E', 'S') => {
                                additional_sel.es =
                                    Some(self.tokens.take_str_map(4, u16_from_hex).unwrap());
                            }
                            ('S', 'I') => {
                                additional_sel.si =
                                    Some(self.tokens.take_str_map(4, u16_from_hex).unwrap());
                            }
                            ('S', 'F') => {
                                // Flags/Subfunction?
                                let high = self.tokens.take_str_map(2, u8_from_hex).unwrap();
                                if !self.tokens.is_next_char('-') {
                                    let low = self.tokens.take_str_map(2, u8_from_hex).unwrap();
                                    additional_sel.flags = Some((high as u16) << 8 | low as u16);
                                    //} else {
                                    //if self.verbose {
                                    //eprintln!("SF arg ignored");
                                    //}
                                }
                            }
                            ('V', 'x') => {
                                // Some VxD identifier
                                //if self.verbose {
                                //eprintln!("Ignoring Vx register constraint");
                                //}
                                self.tokens.take_str(4);
                            }
                            _ => todo!(
                                "Register {}{} (offs {})",
                                reg,
                                reg_size,
                                self.tokens.current_offset()
                            ),
                        }
                    }
                    self.tokens.skip_line();
                    self.state = ParseState::Breadcrumb;
                }
                ParseState::Breadcrumb => {
                    let section = section.as_mut().unwrap();
                    let line = self.tokens.take_line();
                    let line = line.trim();

                    let mut split = line.split(" - ");
                    let int_desc = split.next().unwrap();
                    let int_desc = int_desc.trim();
                    let breadcrumb = split.map(|x| x.to_owned()).collect::<Vec<_>>();

                    assert_eq!(&int_desc[0..4], "INT ");
                    let int_desc = &int_desc[4..];

                    let hex_code = u8_from_hex(&int_desc[..2]).unwrap();
                    assert_eq!(hex_code, section.interrupt);
                    let mut int_desc = &int_desc[2..];

                    if !int_desc.is_empty() && int_desc.starts_with('h') {
                        int_desc = &int_desc[1..];
                    }

                    let int_desc = int_desc.trim_start();
                    if !int_desc.is_empty() {
                        let tags = int_desc
                            .chars()
                            .map(|c| Tag::from_char(c).unwrap())
                            .collect::<Vec<_>>();
                        section.tags = tags;
                    }

                    section.breadcrumb = breadcrumb;
                    self.state = ParseState::RegStackContent;
                }
                ParseState::RegStackContent => {
                    if self.tokens.eat_char('\t')
                        || self.tokens.eat_char(' ')
                        || self.tokens.is_next_str("STACK")
                    {
                        let content = self.tokens.take_line();
                        let content = content.trim();
                        let rsc = section
                            .as_mut()
                            .unwrap()
                            .reg_stack_content
                            .get_or_insert_with(Vec::new);
                        rsc.push(content.to_owned());
                    } else if self.tokens.is_next_str(SECTION_HEADER_START) {
                        self.state = ParseState::SectionHeader;
                    } else if self.tokens.is_next_str("--") {
                        self.tokens.skip_line();
                    } else {
                        self.state = ParseState::FieldName;
                    }
                }
                ParseState::FieldName => {
                    if self.tokens.is_next_str(SECTION_HEADER_START) {
                        self.state = ParseState::SectionHeader;
                        continue;
                    } else if self.tokens.is_next_str("--") || self.tokens.is_next_str("!!!") {
                        self.tokens.skip_line();
                        continue;
                    }

                    let line = self.tokens.take_line();
                    let line = line.trim();

                    if line.is_empty() {
                        self.state = ParseState::TableStart;
                        continue;
                    }

                    //eprintln!("ln {} {}", line.trim(), self.tokens.current_offset());

                    let mut fd = line.splitn(2, ':');

                    let field_key = fd.next().unwrap();
                    let field_key = FieldKey::from_str(field_key).ok_or_else(|| {
                        ParseError::KeyNotInCollection {
                            key: field_key.into(),
                        }
                    })?;

                    let field_text = fd.next().unwrap();
                    if !field_text.is_empty() {
                        let field_text = &field_text[1..];

                        section
                            .as_mut()
                            .unwrap()
                            .fields
                            .append_line(field_key, field_text)?;
                    }

                    last_field_type = Some(field_key);
                    self.state = ParseState::FieldText;
                }
                ParseState::FieldText => {
                    if self.tokens.eat_char('\t') || /* Thanks, Matthias Paul */ self.tokens.eat_char(' ')
                    {
                        let continuation = self.tokens.take_line();
                        let continuation = continuation.trim_end();
                        if continuation.trim().starts_with("!!!") {
                            // Some comment-like thing
                            continue;
                        }

                        let last_field_type = last_field_type.unwrap();

                        let section = section.as_mut().unwrap();
                        if continuation.starts_with("  ") {
                            // Two spaces -> continue appending to last paragraph
                            section
                                .fields
                                .append_continuation(last_field_type, &continuation[1..])?;
                        } else {
                            // Begin new paragraph
                            section.fields.append_line(last_field_type, continuation)?;
                        }
                    } else if self.tokens.is_next_str(SECTION_HEADER_START) {
                        last_field_type = None;
                        self.state = ParseState::SectionHeader;
                    } else if self.tokens.is_next_str("--") {
                        self.tokens.skip_line();
                        continue;
                    } else if self.tokens.eat_empty_line() {
                        last_field_type = None;
                        let section = section.as_ref().unwrap();
                        let add_sel = section.additional_selector.as_ref().unwrap();
                        if section.interrupt == 0x2F
                            && add_sel.ah == Some(0x1A)
                            && add_sel.al == Some(0xA6)
                            && section.fields.returns.is_some()
                            && section.fields.see_also.is_some()
                            && section.fields.description.is_none()
                        {
                            // There is a newline there, but fields still continue
                            self.state = ParseState::FieldName;
                        } else {
                            self.state = ParseState::TableStart;
                        }
                    } else if self.tokens.is_next_str("INT")
                        && section.as_ref().unwrap().interrupt == 0x62
                    {
                        let line = self.tokens.take_line();
                        let line = line.trim();

                        let section = section.as_mut().unwrap();

                        let mut vals = line.split(',').map(|x| x.to_owned()).collect::<Vec<_>>();
                        section.fields.see_also.as_mut().unwrap().append(&mut vals);
                    } else {
                        last_field_type = None;
                        self.state = ParseState::FieldName;
                    }
                }
                ParseState::TableStart => {
                    if self.tokens.eat_char('(') {
                        let mut tab = Table::default();
                        self.tokens.expect_str("Table ")?;
                        tab.id = self.tokens.take_while(|c| c != ')').parse().unwrap();
                        self.tokens.expect_char(')')?;
                        self.tokens.expect_empty_line()?;

                        self.state = ParseState::TableTitleNoHeader;
                        table = Some(tab);
                    } else if self.tokens.eat_empty_line() {
                        continue;
                    } else {
                        self.state = ParseState::TableTitle;
                    }
                }
                ParseState::TableTitle | ParseState::TableTitleNoHeader => {
                    let table = table.get_or_insert_with(Table::default);

                    table.parent_item = section.as_ref().unwrap().entry_id;

                    let mut title = self.tokens.take_line();
                    if title.ends_with(':') {
                        title = &title[..title.len() - 1];
                    }
                    table.title = title.to_owned();

                    self.state = if self.state == ParseState::TableTitle {
                        ParseState::TableHeader
                    } else {
                        ParseState::TableBody
                    };
                }
                ParseState::TableHeader => {
                    let table = table.as_mut().unwrap();
                    let header = table.header.get_or_insert_with(Vec::new);

                    let header_widths = table_header_widths.get_or_insert_with(Vec::new);

                    loop {
                        if self.tokens.eat_str("(Table ") {
                            break;
                        } else {
                            let col_header = self.tokens.take_while(|c| c != '\t' && c != '\n');
                            self.tokens.expect_char('\t')?;
                            header.push(col_header.to_owned());

                            let width = col_header.chars().count() as u32;
                            // Tab width in listings is 8.
                            // This rounds to the next multiple of 8.
                            // Size is required for tables which use space as column separators in
                            // their body
                            let width = width + (8 - width % 8);
                            header_widths.push(width);
                        }
                    }

                    let id = self.tokens.take_while(|c| c != ')');
                    table.id = id.parse().unwrap();
                    self.tokens.expect_char(')')?;
                    self.tokens.expect_empty_line()?;

                    self.state = ParseState::TableBody;
                }
                ParseState::TableBody => {
                    if self.tokens.eat_empty_line() {
                        let mut table = table.take().unwrap();
                        if table.id == 3672 {
                            // There is a three-line text block and I don't know where this should
                            // belong to - putting it to the notes of the last table seems like a
                            // good enough workaround
                            table.fields.append_line("Note", self.tokens.take_line())?;
                            table
                                .fields
                                .append_continuation("Note", self.tokens.take_line())?;
                            table
                                .fields
                                .append_continuation("Note", self.tokens.take_line())?;
                            self.tokens.expect_empty_line()?;
                        }
                        self.state = ParseState::TableStart;
                        self.pending_tables.push_back(table);
                        tmode = TableMode::Normal;
                    } else if self.tokens.is_next_str(SECTION_HEADER_START) {
                        self.state = ParseState::SectionHeader;
                        self.pending_tables.push_back(table.take().unwrap());
                        tmode = TableMode::Normal;
                    } else if self.tokens.is_next_char('!') {
                        // This is some kind of comment (TODO found some at the end of the line -
                        // find and fix that)
                        self.tokens.skip_line();
                    } else if self.tokens.eat_str("--") {
                        // Some sub-heading - TODO
                        self.tokens.skip_line();
                        last_row_was_subheading = true;
                    } else if self.tokens.eat_char('\t') {
                        // line starting with tab -> continuation of last entry
                        let table = table.as_mut().unwrap();
                        let last_entry = table.rows.last_mut();
                        if let Some(last_entry) = last_entry {
                            if tmode == TableMode::Weird {
                                let cell = last_entry
                                    .get_mut(0)
                                    .expect("Weird Table Mode parsing failed (no last entry)");

                                let line = self.tokens.take_line();
                                cell.push('\n');
                                cell.push_str(line);
                            } else {
                                // count tabs, this will be the cell index
                                let mut index = 1;
                                while self.tokens.eat_char('\t') {
                                    index += 1;
                                }

                                let line = self.tokens.take_line();
                                let line = line.trim();

                                let cell = if last_entry.len() <= index {
                                    // continuation for row with less columns than this index
                                    // requires - just append to last instead of erroring
                                    last_entry
                                        .last_mut()
                                        .expect("Continuation but last row was empty")
                                } else {
                                    last_entry.get_mut(index).unwrap()
                                };

                                cell.push('\n');
                                cell.push_str(line);
                            }
                        } else {
                            // This is some weird "table" format, e.g. in INT 0C (INTERRUP.A)
                            // I have no idea how to parse this exactly
                            tmode = TableMode::Weird;
                            let line = self.tokens.take_line();
                            let line = line.trim();
                            let vec = line.split('\t').map(|x| x.to_owned()).collect::<Vec<_>>();
                            table.rows.push(vec);
                        }
                    } else if self.tokens.eat_char(' ')
                        || self.tokens.peek_char().is_numeric()
                        || self.tokens.peek_char() == '-'
                        || table.as_ref().unwrap().id == 1690
                        || !self.tokens.line_contains_char(':')
                    {
                        // Standard table rows should begin with a space - otherwise it's the
                        // start of table fields.
                        // There are still more errors in the listings (TODO find them) where this
                        // isn't the case (missing space, etc.)
                        // If the row starts with a number and not a space, we assume that it still
                        // is a row because a field wouldn't make sense (for whatever reason some
                        // of the numbers are also negative, hence the '-').
                        // For #01690 just force this path; it's just too weird
                        // NEW: If there is no colon in the row, it is probably another row instead
                        // of a field. TODO: check if some other edge cases are solved by this (or
                        // made worse)

                        if self.tokens.eat_empty_line() {
                            // We even got one where there is a space but actually the line is
                            // empty so the next table starts
                            self.state = ParseState::TableStart;
                            continue;
                        }

                        let table = table.as_mut().unwrap();
                        let mut row = Vec::new();
                        let mut cell_index = 0;
                        loop {
                            //let cell_width = table_header_widths.as_ref().map(|x| *x.get(cell_index).unwrap());
                            //let mut counter = 0;
                            let cell = self.tokens.take_while(|chr| {
                                // Hacky workaround - I don't know how do differentiate this
                                // case (maybe there must be a tab before or at the cell width?)
                                // TODO
                                if table.id == 28 && chr == ' ' && cell_index == 0 {
                                    return false;
                                }
                                chr != '\t' && chr != '\r' && chr != '\n'
                            });
                            row.push(cell.to_owned());

                            if self.tokens.eat_char('\r') {
                                self.tokens.eat_char('\n');
                                break;
                            } else if self.tokens.eat_char('\n') {
                                break;
                            } else if table.id == 28 && self.tokens.eat_char(' ') {
                                // do nothing in this case
                            } else {
                                self.tokens.expect_char('\t')?;
                            }

                            cell_index += 1;
                        }

                        table.rows.push(row);
                    } else if last_row_was_subheading {
                        // Table 00414 decides that one of the subsection is just text and thus
                        // doesn't need a space at the start of the first line, which confuses the
                        // table parser
                        // TODO this should probably at some point be a separate state - this is
                        // getting hard to manage
                        last_row_was_subheading = false;
                        let table = table.as_mut().unwrap();

                        let value = self.tokens.take_line();
                        let row = vec![value.to_owned()];
                        table.rows.push(row);
                    } else {
                        let table = table.as_mut().unwrap();
                        if table.id == 1363 {
                            table.rows.push(vec![self
                                .tokens
                                .take_line()
                                .trim_end_matches(':')
                                .into()]);
                        } else if table.id == 1515 && self.tokens.is_next_str("var") {
                            let line = self.tokens.take_line();
                            let line = line.split('\t').map(str::to_owned).collect::<Vec<_>>();
                            table.rows.push(line);
                        } else if table.rows.is_empty() {
                            if table.id == 440 {
                                // WTF is even going on? Should that be a header in #00440 ??
                                self.tokens.skip_line();
                            } else if table.id == 446 {
                                // This is some kind of subtitle - just append this to the title for now
                                table.title.push(' ');
                                table.title.push_str(self.tokens.take_line());
                            } else if table.id == 1636 {
                                // TODO make some subtitle for table, same as above
                                self.tokens.skip_line();
                            } else if table.id == 2114 {
                                // This one actually is just empty - handling this with the current
                                // parser is kind of impossible without conflicts
                                self.state = ParseState::TableField;
                            } else {
                                // These seem to screw up the usual formatting for tables with a header
                                let header = self.tokens.take_line();
                                assert!(
                                    !header.starts_with('('),
                                    "Table headers should not start with '('"
                                );
                                let header =
                                    header.split('\t').map(str::to_owned).collect::<Vec<_>>();
                                assert_eq!(table.header, None);
                                table.header = Some(header);
                            }
                        } else {
                            // Assume that a non-empty row without space/tab is the start of fields
                            // (some edge cases above)
                            self.state = ParseState::TableField;
                        }
                    }
                }
                ParseState::TableField => {
                    if self.tokens.is_next_char('!') {
                        self.tokens.skip_line();
                        continue;
                    } else if self.tokens.is_next_str("--") {
                        // subheading type thingy
                        self.tokens.skip_line();
                        continue;
                    }

                    let field_name = self.tokens.take_while(|c| c != ':' && c != '\n');
                    self.tokens.expect_char(':')?;
                    let field_text = self.tokens.take_line();
                    let field_text = field_text.trim();

                    table
                        .as_mut()
                        .unwrap()
                        .fields
                        .append_line(field_name, field_text)?;

                    last_table_field = Some(field_name.to_owned());

                    self.state = ParseState::TableFieldText;
                }
                ParseState::TableFieldText => {
                    if self.tokens.eat_empty_line() {
                        self.state = ParseState::TableStart;
                        self.pending_tables.push_back(table.take().unwrap());
                        last_table_field = None;
                        tmode = TableMode::Normal;
                    } else if self.tokens.is_next_str(SECTION_HEADER_START) {
                        self.state = ParseState::SectionHeader;
                        self.pending_tables.push_back(table.take().unwrap());
                        last_table_field = None;
                        tmode = TableMode::Normal;
                    } else if self.tokens.is_next_char('!') {
                        self.tokens.skip_line();
                        continue;
                    } else if self.tokens.is_next_str("--") {
                        self.tokens.skip_line();
                        continue;
                    } else if self.tokens.eat_char('\t') {
                        // Field continuation
                        let table = table.as_mut().unwrap();
                        if self.tokens.is_next_str("  ") {
                            self.tokens.expect_char(' ')?;
                            let value = self.tokens.take_line();
                            let value = value.trim_end();
                            table.fields.append_continuation(last_table_field.as_ref().unwrap(), value)?;
                        } else {
                            let value = self.tokens.take_line();
                            let value = value.trim_end();
                            table.fields.append_line(last_table_field.as_ref().unwrap(), value)?;
                        }
                    } else if self.tokens.is_next_char(' ') {
                        // Whatever here we go, nobody likes tables
                        let value = self.tokens.take_line();
                        let value = value.trim();
                        let table = table.as_mut().unwrap();
                        table.fields.append_line(last_table_field.as_ref().unwrap(), value)?;
                    } else if tmode == TableMode::WeAreInFieldsButWhyIsThereIndentMissingAndItIsAlsoStartedByLBracketsWhyyyy {
                        let chr = self.tokens.peek_char();
                        let table = table.as_mut().unwrap();
                        // '[' and ' ' -> continue parsing text - we are still in "footnote"
                        // mode or whatever
                        if chr == '[' {
                            let value = self.tokens.take_line();
                            let value = value.trim_end();
                            table.fields.append_line(last_table_field.as_ref().unwrap(), value)?;
                        } else if chr == ' ' {
                            let value = self.tokens.take_line();
                            let value = value.trim();
                            table.fields.append_continuation(last_table_field.as_ref().unwrap(), value)?;
                        } else {
                            self.state = ParseState::TableField;
                            last_table_field = None;
                            tmode = TableMode::Normal;
                            // end that sh*t
                        }
                    } else if self.tokens.peek_char() == '[' {
                        tmode = TableMode::WeAreInFieldsButWhyIsThereIndentMissingAndItIsAlsoStartedByLBracketsWhyyyy;
                        continue;
                    } else {
                        self.state = ParseState::TableField;
                        last_table_field = None;
                    }
                }
            }
        }

        if let Some(table) = table.take() {
            self.pending_tables.push_back(table);
        }

        if let Some(section) = section.take() {
            return Ok(Some(Item::Interrupt(section)));
        }
        if let Some(table) = self.pending_tables.pop_front() {
            return Ok(Some(Item::Table(table)));
        }
        Ok(None)
    }
}

impl<'a> Iterator for Parser<'a> {
    type Item = Result<Item, ErrorWithState<ParseError>>;

    fn next(&mut self) -> Option<Self::Item> {
        self.do_parse_section()
            .map_err(|e| ErrorWithState {
                inner: e,
                offset: self.tokens.current_offset(),
                state_info: format!("{:?}", self.state),
            })
            .transpose()
    }
}
