use crate::error::ParseError;

pub struct Tokenizer<'a> {
    orig_buf: &'a str,
    buf: &'a str,
}

impl<'a> Tokenizer<'a> {
    pub fn new(buf: &'a str) -> Self {
        Self { orig_buf: buf, buf }
    }

    #[inline]
    pub fn current_offset(&self) -> usize {
        self.orig_buf.len() - self.buf.len()
    }

    fn advance(&mut self, bytes: usize) {
        self.buf = &self.buf[bytes..];
    }

    pub fn has_more(&self) -> bool {
        !self.buf.is_empty()
    }
    pub fn is_next_str(&self, v: &str) -> bool {
        self.buf.starts_with(v)
    }
    pub fn is_next_char(&self, ch: char) -> bool {
        self.buf.starts_with(ch)
    }
    pub fn skip_line(&mut self) {
        for (i, chr) in self.buf.char_indices() {
            if chr == '\n' {
                self.buf = &self.buf[i + 1..];
                return;
            }
        }
        self.buf = &self.buf[self.buf.len() - 1..];
    }
    pub fn line_contains_char(&self, wanted: char) -> bool {
        for chr in self.buf.chars() {
            if chr == '\n' || chr == '\r' {
                return false;
            } else if chr == wanted {
                return true;
            }
        }
        false
    }
    pub fn expect_char(&mut self, c: char) -> Result<(), ParseError> {
        if !self.buf.starts_with(c) {
            Err(ParseError::UnexpectedCharacter {
                expected: c,
                actual: self.peek_char(),
            })
        } else {
            self.advance(c.len_utf8());
            Ok(())
        }
    }
    pub fn expect_str(&mut self, v: &str) -> Result<(), ParseError> {
        if !self.buf.starts_with(v) {
            Err(ParseError::UnexpectedString {
                expected: v.to_owned(),
                actual: self.buf[..v.len()].to_owned(),
            })
        } else {
            self.advance(v.len());
            Ok(())
        }
    }
    pub fn eat_one_or_more(&mut self, v: char) -> Result<(), ParseError> {
        let mut offs = 0;
        for (i, chr) in self.buf.char_indices() {
            if chr != v {
                offs = i;
                break;
            }
        }
        if offs == 0 {
            return Err(ParseError::NoCharOccurrenceFound {
                expected: v,
                actual: self.peek_char(),
            });
        }
        self.advance(offs);
        Ok(())
    }
    pub fn eat_char(&mut self, v: char) -> bool {
        if self.buf.starts_with(v) {
            self.advance(v.len_utf8());
            true
        } else {
            false
        }
    }
    pub fn eat_str(&mut self, v: &str) -> bool {
        if self.buf.starts_with(v) {
            self.advance(v.len());
            true
        } else {
            false
        }
    }
    pub fn take_char(&mut self) -> char {
        let chr = self.buf.chars().next().unwrap();
        self.advance(chr.len_utf8());
        chr
    }
    pub fn take_str(&mut self, count: usize) -> &'a str {
        let (byte_len, _) = self.buf.char_indices().nth(count).unwrap();
        let offs = self.current_offset();
        let str_view = &self.orig_buf[offs..offs + byte_len];
        self.advance(byte_len);
        str_view
    }
    pub fn take_str_map<T, F: Fn(&str) -> T>(&mut self, count: usize, mapper: F) -> T {
        mapper(self.take_str(count))
    }
    pub fn take_while<F: FnMut(char) -> bool>(&mut self, mut predicate: F) -> &'a str {
        let offs = self
            .buf
            .char_indices()
            .skip_while(|(_, c)| predicate(*c))
            .map(|(i, _)| i)
            .next()
            .unwrap();

        let coff = self.current_offset();
        let strng = &self.orig_buf[coff..coff + offs];
        self.advance(offs);

        strng
    }
    pub fn take_line(&mut self) -> &'a str {
        let strng = self.take_while(|c| c != '\n');
        self.advance(1);
        if strng.ends_with('\r') {
            &strng[..strng.len() - 1]
        } else {
            strng
        }
    }
    pub fn eat_empty_line(&mut self) -> bool {
        if self.buf.starts_with("\r\n") {
            self.advance(2);
            true
        } else if self.buf.starts_with('\n') {
            self.advance(1);
            true
        } else {
            false
        }
    }
    pub fn expect_empty_line(&mut self) -> Result<(), ParseError> {
        if !self.eat_empty_line() {
            Err(ParseError::ExpectedEmptyLine {
                actual: self.peek_char(),
            })
        } else {
            Ok(())
        }
    }
    pub fn peek_char(&self) -> char {
        self.buf.chars().next().unwrap()
    }
}
