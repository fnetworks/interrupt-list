use super::fields::FieldCollection;
use serde::Serialize;

#[derive(Debug, Default, Serialize)]
pub struct TableFields {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub see_also: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notes: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub returns: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub index: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bugs: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub program: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub legend: Option<String>,
}

impl FieldCollection<&str> for TableFields {
    fn string_field_mut(&mut self, key: &str) -> Option<&mut Option<String>> {
        match key {
            "Note" | "Notes" => Some(&mut self.notes),
            "Return" | "Returns" => Some(&mut self.returns),
            "Index" => Some(&mut self.index),
            "Desc" => Some(&mut self.description),
            "BUGs" | "BUG" => Some(&mut self.bugs),
            "Program" => Some(&mut self.program),
            "Legend" => Some(&mut self.legend),
            _ => None,
        }
    }
    fn vec_field_mut(&mut self, key: &str) -> Option<&mut Option<Vec<String>>> {
        match key {
            "SeeAlso" => Some(&mut self.see_also),
            _ => None,
        }
    }
}

#[derive(Debug, Default, Serialize)]
pub struct Table {
    pub id: u32,
    pub parent_item: u32,
    pub title: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub header: Option<Vec<String>>,
    pub rows: Vec<Vec<String>>,
    pub fields: TableFields,
}

#[cfg(test)]
mod tests {
    use super::{FieldCollection, TableFields};

    #[test]
    fn table_fields_append_see_also() {
        let mut table_fields = TableFields::default();

        assert_eq!(table_fields.see_also, None);

        table_fields.append_line("SeeAlso", "#1,#2").unwrap();

        {
            let see_also = table_fields
                .see_also
                .as_ref()
                .expect("see_also should not be None");
            assert_eq!(see_also.len(), 2);
            assert_eq!(see_also[0], "#1");
            assert_eq!(see_also[1], "#2");
        }

        table_fields.append_line("SeeAlso", "#3,#4").unwrap();

        {
            let see_also = table_fields
                .see_also
                .as_ref()
                .expect("see_also should not be None");
            assert_eq!(see_also.len(), 4);
            assert_eq!(see_also[0], "#1");
            assert_eq!(see_also[1], "#2");
            assert_eq!(see_also[2], "#3");
            assert_eq!(see_also[3], "#4");
        }
    }

    #[test]
    fn table_fields_append_notes() {
        let mut table_fields = TableFields::default();

        assert_eq!(table_fields.notes, None);

        table_fields
            .append_line("Notes", "First line of note")
            .unwrap();

        {
            let notes = table_fields
                .notes
                .as_ref()
                .expect("notes should not be None");
            assert_eq!(notes, "First line of note");
        }

        table_fields
            .append_line("Note", "Second line of note")
            .unwrap();

        {
            let notes = table_fields
                .notes
                .as_ref()
                .expect("notes should not be None");
            assert_eq!(notes, "First line of note\nSecond line of note");
        }
    }
}
