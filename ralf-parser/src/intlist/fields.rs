use crate::error::{ParseError, ParseResult};

pub trait FieldCollection<K: Copy + PartialEq + std::fmt::Debug> {
    fn string_field_mut(&mut self, key: K) -> Option<&mut Option<String>>;
    fn vec_field_mut(&mut self, key: K) -> Option<&mut Option<Vec<String>>>;

    fn append_line(&mut self, key: K, value: &str) -> ParseResult<()> {
        match self.string_field_mut(key) {
            Some(field) => match field {
                Some(ref mut field) => {
                    field.push('\n');
                    field.push_str(value);
                }
                None => {
                    *field = Some(value.to_owned());
                }
            },
            None => {
                let field =
                    self.vec_field_mut(key)
                        .ok_or_else(|| ParseError::KeyNotInCollection {
                            key: format!("{:?}", key),
                        })?;
                let mut values = value
                    .split(|c| c == ',' || c == ';')
                    .map(|x| x.to_owned())
                    .collect::<Vec<_>>();
                field.get_or_insert_with(Vec::new).append(&mut values);
            }
        }
        Ok(())
    }

    fn append_continuation(&mut self, key: K, value: &str) -> ParseResult<()> {
        let field = self.string_field_mut(key).ok_or_else(|| {
            ParseError::ContinuationForNonStringField {
                key: value.to_owned(),
            }
        })?;
        let text = field
            .as_mut()
            .ok_or_else(|| ParseError::ContinuationForEmptyField {
                key: value.to_owned(),
            })?;
        text.push_str(value);
        Ok(())
    }
}
