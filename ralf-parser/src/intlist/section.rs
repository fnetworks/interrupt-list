use super::fields::FieldCollection;
use super::Category;

use serde::Serialize;

#[derive(Debug, Default, Serialize)]
pub struct AdditionalSelector {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ah: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub al: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bh: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bl: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ch: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cl: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dh: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dl: Option<u8>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub flags: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bp: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub di: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub es: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ds: Option<u16>,
}

#[derive(Debug, Default, Serialize)]
pub struct Fields {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notes: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub returns: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bugs: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub program: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub install_check: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub index: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub see_also: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub example: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub range: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub warning: Option<String>,
}

impl FieldCollection<FieldKey> for Fields {
    /// Gets the field for the specified key as a mutable reference. Returns `None` if the field is
    /// not a string key.
    fn string_field_mut(&mut self, key: FieldKey) -> Option<&mut Option<String>> {
        Some(match key {
            FieldKey::Description => &mut self.description,
            FieldKey::Notes => &mut self.notes,
            FieldKey::Returns => &mut self.returns,
            FieldKey::Bugs => &mut self.bugs,
            FieldKey::Program => &mut self.program,
            FieldKey::InstallCheck => &mut self.install_check,
            FieldKey::Index => &mut self.index,
            FieldKey::Example => &mut self.example,
            FieldKey::Range => &mut self.range,
            FieldKey::Warning => &mut self.warning,

            FieldKey::SeeAlso => return None,
        })
    }

    fn vec_field_mut(&mut self, key: FieldKey) -> Option<&mut Option<Vec<String>>> {
        match key {
            FieldKey::SeeAlso => Some(&mut self.see_also),
            _ => None,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum FieldKey {
    Description,
    Notes,
    Returns,
    Bugs,
    Program,
    InstallCheck,
    Index,
    SeeAlso,
    Example,
    Range,
    Warning,
}

impl FieldKey {
    pub(crate) fn from_str(key: &str) -> Option<Self> {
        Some(match key {
            "Desc" => Self::Description,
            "Note" | "Notes" => Self::Notes,
            "Return" => Self::Returns,
            "BUG" | "BUGS" => Self::Bugs,
            "Program" => Self::Program,
            "InstallCheck" => Self::InstallCheck,
            "Index" => Self::Index,
            "Example" => Self::Example,
            "SeeAlso" | "SeeALso" | "See also" => Self::SeeAlso,
            "Range" => Self::Range,
            "Warning" => Self::Warning,
            _ => return None,
        })
    }
}

#[derive(Copy, Clone, Debug, Serialize)]
pub enum Tag {
    Undocumented,
    PartiallyDocumented,
    AvailableOnlyInProtectedMode,
    AvailableOnlyInRealOrV86Mode,
    CalloutOrCallback,
    Obsolete,
}

impl Tag {
    pub(crate) fn from_char(key: char) -> Option<Self> {
        Some(match key {
            'U' => Self::Undocumented,
            'u' => Self::PartiallyDocumented,
            'P' => Self::AvailableOnlyInProtectedMode,
            'R' => Self::AvailableOnlyInRealOrV86Mode,
            'C' => Self::CalloutOrCallback,
            'O' => Self::Obsolete,
            _ => return None,
        })
    }
}

#[derive(Debug, Serialize)]
pub struct Section {
    pub entry_id: u32,
    pub category: Category,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub tags: Vec<Tag>,
    pub breadcrumb: Vec<String>,
    pub interrupt: u8,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub additional_selector: Option<AdditionalSelector>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reg_stack_content: Option<Vec<String>>,
    pub fields: Fields,
}

#[cfg(test)]
mod tests {
    use super::{FieldCollection, FieldKey, Fields};
    #[test]
    fn fields_append_see_also() {
        let mut fields = Fields::default();

        assert_eq!(fields.see_also, None);

        fields.append_line(FieldKey::SeeAlso, "#1,#2").unwrap();

        {
            let see_also = fields
                .see_also
                .as_ref()
                .expect("see_also should not be None");
            assert_eq!(see_also.len(), 2);
            assert_eq!(see_also[0], "#1");
            assert_eq!(see_also[1], "#2");
        }

        fields.append_line(FieldKey::SeeAlso, "#3,#4").unwrap();

        {
            let see_also = fields
                .see_also
                .as_ref()
                .expect("see_also should not be None");
            assert_eq!(see_also.len(), 4);
            assert_eq!(see_also[0], "#1");
            assert_eq!(see_also[1], "#2");
            assert_eq!(see_also[2], "#3");
            assert_eq!(see_also[3], "#4");
        }
    }

    #[test]
    #[should_panic]
    fn fields_append_see_also_continuation_should_panic() {
        let mut fields = Fields::default();
        fields.append_line(FieldKey::SeeAlso, "#1,#2").unwrap();
        fields.append_continuation(FieldKey::SeeAlso, "#3").unwrap();
    }

    #[test]
    fn fields_append_notes() {
        let mut fields = Fields::default();

        assert_eq!(fields.notes, None);

        fields
            .append_line(FieldKey::Notes, "First paragraph of note")
            .unwrap();

        {
            let notes = fields.notes.as_ref().expect("notes should not be None");
            assert_eq!(notes, "First paragraph of note");
        }

        fields
            .append_line(FieldKey::Notes, "Second paragraph of note")
            .unwrap();

        {
            let notes = fields.notes.as_ref().expect("notes should not be None");
            assert_eq!(notes, "First paragraph of note\nSecond paragraph of note");
        }

        fields
            .append_continuation(FieldKey::Notes, " and some more text")
            .unwrap();

        {
            let notes = fields.notes.as_ref().expect("notes should not be None");
            assert_eq!(
                notes,
                "First paragraph of note\nSecond paragraph of note and some more text"
            );
        }
    }
}
