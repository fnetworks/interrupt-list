use ralf_parser::{Item, Parser};

use std::fs;
use std::path::PathBuf;

use clap::{App, Arg};
use rusqlite::params;

mod db;
use db::TableWriter;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("ralf-parser")
        .arg(
            Arg::with_name("output")
                .long("output")
                .short("o")
                .help("The main output file")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("INPUTS")
                .help("The input listings")
                .required(true)
                .multiple(true),
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .help("Print additional information"),
        )
        .arg(
            Arg::with_name("dry_run")
                .long("dry")
                .help("Don't output anything, only parse"),
        )
        .get_matches();

    let output = PathBuf::from(matches.value_of("output").unwrap());
    let inputs = matches.values_of("INPUTS").unwrap().map(PathBuf::from);
    let verbose = matches.is_present("verbose");
    let dry_run = matches.is_present("dry_run");

    let output = if !dry_run {
        Some(TableWriter::create_database(&output)?)
    } else {
        None
    };

    let mut writer = output.as_ref().map(TableWriter::new).transpose()?;

    if let Some(ref output) = output {
        output.execute("BEGIN", params![])?;
    }

    let mut counter = 0;
    for input in inputs {
        if verbose {
            eprintln!("Processing {:?} ...", input);
        }
        let buf = fs::read_to_string(input).unwrap();
        let parser = Parser::new(&buf, verbose, counter);
        for item in parser {
            let mut item = item.unwrap();
            match &mut item {
                Item::Interrupt(_) => {
                    counter += 1;
                }
                Item::Table(tab) => {
                    if tab.id == 1213
                        && tab.title == "Bitfields for Intel 82439TX DRAM Row Type high register"
                    {
                        // This table has the wrong number (and thus collides with another),
                        // it should most likely be 1120
                        tab.id = 1120;
                    } else if tab.id == 90010
                        && tab.title
                            == "Format of ASPI2DOS.SYS v3.65 host adapter unique parameters"
                    {
                        // ID 90010 is already taken, this is the most plausible next ID
                        tab.id = 90012
                    }
                }
            }
            if let Some(ref mut writer) = writer {
                writer.insert(item)?
            }
        }
    }

    if let Some(ref output) = output {
        output.execute("COMMIT", params![])?;
    }

    Ok(())
}
