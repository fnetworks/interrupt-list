use ralf_parser::Item;
use rusqlite::{params, Connection, Statement};
use serde_json;
use std::path::Path;

pub struct TableWriter<'a> {
    insert_interrupt_stmt: Statement<'a>,
    insert_table_stmt: Statement<'a>,
}

impl<'a> TableWriter<'a> {
    pub fn create_database(path: &Path) -> Result<Connection, rusqlite::Error> {
        if path.exists() {
            eprintln!("Error: The specified output file already exists.");
            std::process::exit(1);
        }

        let output = Connection::open(path)?;

        output.execute_batch(
            r#"
                BEGIN;
                CREATE TABLE "Interrupts" (
                    "entry_id"	            INTEGER NOT NULL UNIQUE,
                    "interrupt"	            INTEGER NOT NULL,
                    "category"	            TEXT NOT NULL,
                    "tags"                  TEXT,
                    "breadcrumb"	        TEXT NOT NULL,
                    "additional_selector"	TEXT,
                    "reg_stack_content"	    TEXT,
                    "field__description"	TEXT,
                    "field__notes"	        TEXT,
                    "field__returns"	    TEXT,
                    "field__bugs"	        TEXT,
                    "field__program"	    TEXT,
                    "field__install_check"	TEXT,
                    "field__index"	        TEXT,
                    "field__see_also"	    TEXT,
                    "field__example"	    TEXT,
                    "field__range"	        TEXT,
                    "field__warning"	    TEXT,
                    PRIMARY KEY("entry_id")
                );
                CREATE TABLE "Tables" (
                    "id"	                INTEGER NOT NULL UNIQUE,
                    "parent_item"	        INTEGER NOT NULL,
                    "title"	                TEXT NOT NULL,
                    "header"	            TEXT,
                    "rows"	                TEXT NOT NULL,
                    "field__see_also"       TEXT,
                    "field__notes"          TEXT,
                    "field__returns"        TEXT,
                    "field__index"          TEXT,
                    "field__description"    TEXT,
                    "field__bugs"           TEXT,
                    "field__program"        TEXT,
                    "field__legend"         TEXT,
                    PRIMARY KEY("id")
                );
                COMMIT;
            "#,
        )?;
        Ok(output)
    }

    pub fn new(conn: &'a Connection) -> Result<Self, rusqlite::Error> {
        Ok(Self {
            insert_interrupt_stmt: conn.prepare(
                "INSERT INTO Interrupts \
                    (entry_id, interrupt, category, tags, breadcrumb, additional_selector, \
                    reg_stack_content, field__description, field__notes, field__returns, field__bugs, \
                    field__program, field__install_check, field__index, field__see_also, field__example, \
                    field__range, field__warning) \
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            )?,
            insert_table_stmt: conn.prepare(
                "INSERT INTO Tables \
                    (id, parent_item, title, header, rows, field__see_also, field__notes, \
                    field__returns, field__index, field__description, field__bugs, field__program, \
                    field__legend) \
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            )?,
        })
    }

    pub fn insert(&mut self, item: Item) -> Result<(), Box<dyn std::error::Error>> {
        match item {
            Item::Interrupt(int) => {
                self.insert_interrupt_stmt.execute(params![
                    int.entry_id,
                    int.interrupt,
                    int.category.key(),
                    serde_json::to_string(&int.tags)?,
                    serde_json::to_string(&int.breadcrumb)?,
                    int.additional_selector
                        .as_ref()
                        .map(serde_json::to_string)
                        .transpose()?,
                    int.reg_stack_content
                        .as_ref()
                        .map(serde_json::to_string)
                        .transpose()?,
                    int.fields.description,
                    int.fields.notes,
                    int.fields.returns,
                    int.fields.bugs,
                    int.fields.program,
                    int.fields.install_check,
                    int.fields.index,
                    int.fields
                        .see_also
                        .as_ref()
                        .map(serde_json::to_string)
                        .transpose()?,
                    int.fields.example,
                    int.fields.range,
                    int.fields.warning,
                ])?;
            }
            Item::Table(tab) => {
                self.insert_table_stmt.execute(params![
                    tab.id,
                    tab.parent_item,
                    tab.title,
                    tab.header.as_ref().map(serde_json::to_string).transpose()?,
                    serde_json::to_string(&tab.rows)?,
                    tab.fields
                        .see_also
                        .as_ref()
                        .map(serde_json::to_string)
                        .transpose()?,
                    tab.fields.notes,
                    tab.fields.returns,
                    tab.fields.index,
                    tab.fields.description,
                    tab.fields.bugs,
                    tab.fields.program,
                    tab.fields.legend,
                ])?;
            }
        }
        Ok(())
    }
}
