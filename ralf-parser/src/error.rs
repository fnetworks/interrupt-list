use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult};
use std::num::ParseIntError;

#[derive(Debug, Clone)]
pub enum ParseError {
    UnexpectedCharacter { expected: char, actual: char },
    UnexpectedString { expected: String, actual: String },
    NoCharOccurrenceFound { expected: char, actual: char },
    ExpectedEmptyLine { actual: char },
    KeyNotInCollection { key: String },
    ContinuationForNonStringField { key: String },
    ContinuationForEmptyField { key: String },
    IntegerParseError(ParseIntError),
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Self::UnexpectedCharacter { expected, actual } => {
                write!(f, "Expected {:?}, but got {:?}", expected, actual)
            }
            Self::UnexpectedString { expected, actual } => {
                write!(f, "Expected {:?}, but got {:?}", expected, actual)
            }
            Self::NoCharOccurrenceFound { expected, actual } => write!(
                f,
                "Expected at least one occurrence of {:?}, but got {:?}",
                expected, actual
            ),
            Self::ExpectedEmptyLine { actual } => {
                write!(f, "Expected empty line, but got {:?}", actual)
            }
            Self::KeyNotInCollection { key } => {
                write!(f, "Key {} not found in field collection", key)
            }
            Self::ContinuationForNonStringField { key } => write!(
                f,
                "Continuation for non-string field {} not implemented",
                key
            ),
            Self::ContinuationForEmptyField { key } => {
                write!(f, "Continuation for empty field {}", key)
            }
            Self::IntegerParseError(err) => write!(f, "Failed to parse integer: {}", err),
        }
    }
}

impl Error for ParseError {}

impl From<ParseIntError> for ParseError {
    fn from(e: ParseIntError) -> Self {
        Self::IntegerParseError(e)
    }
}

pub type ParseResult<T> = Result<T, ParseError>;

#[derive(Debug, Clone)]
pub struct ErrorWithState<E: Error> {
    pub inner: E,
    pub offset: usize,
    pub state_info: String,
}

impl<E: Error> Display for ErrorWithState<E> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(
            f,
            "At offset {}: {} (state: {})",
            self.offset, self.inner, self.state_info
        )
    }
}

impl<E: Error + 'static> Error for ErrorWithState<E> {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.inner)
    }
}
