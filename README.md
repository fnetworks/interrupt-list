# interrupt-list

This repository currently contains:
* a parser for The x86 Interrupt List
* a html page generator

The rendered html can be seen at https://fnetworks.gitlab.io/interrupt-list/

## Licensing

The entirity of the parser/generator ([extraction/](/extraction), [html-generator/](/html-generator), [ralf-parser/](/ralf-parser))
is MIT licensed (see [LICENSE](/LICENSE)).

[intprint.patch](/patches/intprint.patch) is a patch for the `INTPRINT` software by Ralf Brown, which is public domain.
The patch itself is likewise donated to the public domain (No warranty provided).
