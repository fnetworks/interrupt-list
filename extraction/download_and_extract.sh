#!/usr/bin/env bash
set -e

base_url="http://www.cs.cmu.edu/~ralf/interrupt-list"
temp_dir=temp

a_files=(INTERRUP.A INTERRUP.B INTERRUP.C INTERRUP.D)
b_files=(INTERRUP.E INTERRUP.F INTERRUP.G INTERRUP.H INTERRUP.I)
c_files=(INTERRUP.J INTERRUP.K INTERRUP.L INTERRUP.M INTERRUP.N)
d_files=(INTERRUP.O INTERRUP.P INTERRUP.Q INTERRUP.R)
e_files=()
f_files=()
all_files=("${a_files[@]}" "${b_files[@]}" "${c_files[@]}" "${d_files[@]}" "${e_files[@]}" "${f_files[@]}")

out_dir=.

while getopts "o:" opt; do
	case $opt in
		o)
			out_dir=$OPTARG
			mkdir -p "$out_dir"
			;;
		*)
			echo "Unexpected argument"
			exit 1
			;;
	esac
done

# -p is intentionally not given, as this script should exit if 'temp' exists.
mkdir $temp_dir

echo "-- DOWNLOADING ARCHIVES --"
echo "[1/6] inter61a"
curl -sS -o $temp_dir/inter61a.zip "$base_url/inter61a.zip"
echo "[2/6] inter61b"
curl -sS -o $temp_dir/inter61b.zip "$base_url/inter61b.zip"
echo "[3/6] inter61c"
curl -sS -o $temp_dir/inter61c.zip "$base_url/inter61c.zip"
echo "[4/6] inter61d"
curl -sS -o $temp_dir/inter61d.zip "$base_url/inter61d.zip"
echo "[5/6] inter61e (skipping)"
#curl -sS -o $temp_dir/inter61e.zip "$base_url/inter61e.zip"
echo "[6/6] inter61f (skipping)"
#curl -sS -o $temp_dir/inter61f.zip "$base_url/inter61f.zip"


echo
echo "-- EXTRACTING ARCHIVES --"

echo "[1/6] inter61a"
unzip -nq $temp_dir/inter61a.zip "${a_files[@]}" -d $temp_dir
echo "[2/6] inter61b"
unzip -nq $temp_dir/inter61b.zip "${b_files[@]}" -d $temp_dir
echo "[3/6] inter61c"
unzip -nq $temp_dir/inter61c.zip "${c_files[@]}" -d $temp_dir
echo "[4/6] inter61d"
unzip -nq $temp_dir/inter61d.zip "${d_files[@]}" -d $temp_dir
echo "[5/6] inter61e (skipping)"
#unzip -nq $temp_dir/inter61e.zip "${e_files[@]}" -d $temp_dir
echo "[6/6] inter61f (skipping)"
#unzip -nq $temp_dir/inter61f.zip "${f_files[@]}" -d $temp_dir

echo
echo "-- CONVERTING FILES TO UTF-8 --"

file_count=${#all_files[@]}
i=0
for f in "${all_files[@]}"; do
	((i=i+1))
	echo "[$i/$file_count] $f"
	iconv -f latin1 -t utf-8 "$temp_dir/$f" -o "$out_dir/$f"
done

rm -r temp
